
PRO_Server = Block2D_Server.pro
PRO_Client = Block2D_Client.pro
PRO_WebClient = Block2D_WebClient.pro

ifeq ($(OS),Windows_NT)
	QMAKE = C:\Qt\5.4\mingw491_32\bin\qmake.exe
else
	QMAKE = qmake
endif

all: | server client

clean:
	rm -fr QtMakefile QtMakefile.Debug QtMakefile.Release debug release block2d-server block2d block2d.bc

rc:
	./genrc.sh

QtMakefile_Server:
	$(QMAKE) -o QtMakefile $(PRO_Server) CONFIG+=debug_and_release

QtMakefile_Client:
	$(QMAKE) -o QtMakefile $(PRO_Client) CONFIG+=debug_and_release

QtMakefile_WebClient:
	$(QMAKE) -o QtMakefile $(PRO_WebClient) CONFIG+=debug_and_release

server-debug:	QtMakefile_Server
	$(MAKE) -j 2 -f QtMakefile debug

server-release:	QtMakefile_Server
	$(MAKE) -j 2 -f QtMakefile release

server:	QtMakefile_Server
	$(MAKE) -j 2 -f QtMakefile

client-debug:	QtMakefile_Client
	$(MAKE) -j 2 -f QtMakefile debug

client-release:	QtMakefile_Client
	$(MAKE) -j 2 -f QtMakefile release

client:	QtMakefile_Client
	$(MAKE) -j 2 -f QtMakefile

webclient-debug:	QtMakefile_WebClient
	$(MAKE) -j 2 -f QtMakefile debug
	emcc block2d.bc -o block2d.js

webclient-release:	QtMakefile_Client
	$(MAKE) -j 2 -f QtMakefile release
	emcc block2d.bc -o block2d.js

webclient:	QtMakefile_WebClient
	$(MAKE) -j 2 -f QtMakefile
	emcc block2d.bc -o block2d.js

.PHONY: all clean server-debug server-release server client-debug client-release client webclient webclient-debug webclient-release QtMakefile_Server QtMakefile_Client rc
