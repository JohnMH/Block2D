CONFIG -= qt
win32{
	LIBS += -lws2_32
	TARGET = Block2D_Server
}else{
	TARGET = block2d-server
}
LIBS += -lSDL2 -lSDL2_net

#Server-specific defines
DEFINES += BLOCK2D_SERVER

CONFIG += console
RESOURCES += internalrc.qrc
include(shared.pri)
