/*
 * Copyright 2015 John M. Harris, Jr.
 *
 * This file is part of Block2D.
 *
 * Block2D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Block2D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Block2D.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "QtResourceManager.h"

#ifdef QT_CORE_LIB

#include <QString>
#include <QFile>

namespace block2d{
	QtResourceManager::QtResourceManager(){}
	QtResourceManager::~QtResourceManager(){}

	SDL_RWops* QtResourceManager::getResource(std::string str){
		//First, attempt to load the resource as a Qt resource
		QFile res_file(QString(":/") + QString(str.c_str()));
		if(res_file.exists()){
			if(res_file.open(QIODevice::ReadOnly)){
				int fsize = res_file.size();

				char* tmp = new char[fsize];
				res_file.read(tmp, fsize);
				res_file.close();

				return SDL_RWFromMem((void*)tmp, fsize);
			}
		}

		return SDL_RWFromFile(str.c_str(), "rb");
	}
}

#endif
