/*
 * Copyright 2015 John M. Harris, Jr.
 *
 * This file is part of Block2D.
 *
 * Block2D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Block2D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Block2D.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMESTATE_H_
#define GAMESTATE_H_

#ifdef EMSCRIPTEN
#include <SDL.h>
#include <SDL_keycode.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_keycode.h>
#endif

namespace block2d{
	class Game;

	class GameState{
		public:
			GameState(Game* game);
			virtual ~GameState();

			Game* game();

			//These are all self-explanatory

			virtual void pretick();
			virtual void tick() = 0;
			virtual void posttick();

			virtual void prerender();
			virtual void render() = 0;
			virtual void postrender();

			virtual void resize(int width, int height);

			//Called when we switch to this state.
			virtual void init();

			virtual void keyDown(SDL_Keycode keycode);
			virtual void keyUp(SDL_Keycode keycode);

			/**
			 * This is called with the new GameState, when the Game's GameState changes.
			 */
			virtual void switchedState(GameState* newState);

			virtual void quitRequested();

		private:
			Game* _game;
	};
}

#endif
