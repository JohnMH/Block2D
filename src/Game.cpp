/*
 * Copyright 2015 John M. Harris, Jr.
 *
 * This file is part of Block2D.
 *
 * Block2D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Block2D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Block2D.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Game.h"

#include <cstdlib>

namespace block2d{
	Game::Game(){
		running = true;
		gs = NULL;
		stateInited = true;

		#ifdef QT_CORE_LIB
		res_mgr = new QtResourceManager();
		#else
		res_mgr = new SDLResourceManager();
		#endif

		#ifndef BLOCK2D_SERVER
		_width = 0;
		_height = 0;
		#endif
	}

	Game::~Game(){}

	bool Game::isRunning(){
		return running;
	}

	void Game::stop(){
		if(gs){
			gs->quitRequested();
		}
		running = false;
	}

	void Game::tick(){
		if(!stateInited){
			if(gs){
				gs->init();
			}
			stateInited = true;
		}

		if(gs){// We separate these for simplicity, and modularity.
			gs->pretick();
			gs->tick();
			gs->posttick();
		}
	}

	void Game::render(){
		if(gs){//Same deal as above.
			gs->prerender();
			gs->render();
			gs->postrender();
		}
	}

	#ifndef BLOCK2D_SERVER
	void Game::resize(int width, int height){
		_width = width;
		_height = height;

		if(gs){
			gs->resize(width, height);
		}
	}

	int Game::width(){
		return _width;
	}

	int Game::height(){
		return _height;
	}
	#endif

	GameState* Game::state(){
		return gs;
	}

	GameState* Game::swapState(GameState* newState){
		if(gs){
			gs->switchedState(newState);
		}

		GameState* oldState = gs;

		gs = newState;
		stateInited = false;

		return oldState;
	}

	ResourceManager* Game::getResourceManager(){
		return res_mgr;
	}
}
