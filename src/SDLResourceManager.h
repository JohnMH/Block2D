/*
 * Copyright 2015 John M. Harris, Jr.
 *
 * This file is part of Block2D.
 *
 * Block2D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Block2D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Block2D.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SDLRESOURCEMANAGER_H_
#define SDLRESOURCEMANAGER_H_

#include "ResourceManager.h"

namespace block2d{
	class SDLResourceManager: public ResourceManager{
		public:
			SDLResourceManager();
			virtual ~SDLResourceManager();

			virtual SDL_RWops* getResource(std::string str);
	};
}

#endif
