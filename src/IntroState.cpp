/*
 * Copyright 2015 John M. Harris, Jr.
 *
 * This file is part of Block2D.
 *
 * Block2D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Block2D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Block2D.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "IntroState.h"

#ifndef BLOCK2D_SERVER

#include "Game.h"
#include "b2dexception.h"

#ifdef EMSCRIPTEN
#include <SDL_image.h>
#else
#include <SDL2/SDL_image.h>
#endif

#include <iostream>

namespace block2d{
	IntroState::IntroState(Game* gm) : GameState(gm){
		mwr_logo_texture = 0;
		ticks = 0;
	}

	IntroState::~IntroState(){}

	void IntroState::init(){
		if(mwr_logo_texture != 0){
			return;
		}

		SDL_RWops* logo_ops = game()->getResourceManager()->getResource("mwr/mwr_logo.png");
		if(!logo_ops){
			throw b2d_exception("Failed to load `mwr/mwr_logo.png`");
		}

		SDL_Surface* logo_surf = IMG_Load_RW(logo_ops, 1);
		if(!logo_surf){
			throw b2d_exception("Failed to convert data of `mwr/mwr_logo.png` to image");
		}

		//Set up for 2D
		Game* gm = game();
		resize(gm->width(), gm->height());

		glDisable(GL_DEPTH_TEST);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		glColor4f(1, 1, 1, 1);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

		glGenTextures(1, &mwr_logo_texture);

		if(mwr_logo_texture == 0){
			throw b2d_exception("Failed to generate GL texture for `mwr/mwr_logo.png`");
		}

		glBindTexture(GL_TEXTURE_2D, mwr_logo_texture);

		int mode = GL_RGBA;
		glTexImage2D(GL_TEXTURE_2D, 0, mode, logo_surf->w, logo_surf->h, 0, mode, GL_UNSIGNED_BYTE, logo_surf->pixels);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		float bgcolor = 0.105882353;
		glClearColor(bgcolor, bgcolor, bgcolor, 1);
	}

	void IntroState::tick(){
		if(ticks < 400){ //Prevent this overflowing, it's pretty pointless but why not?
			ticks++;
		}
	}

	void IntroState::render(){
		glClear(GL_COLOR_BUFFER_BIT);

		glBindTexture(GL_TEXTURE_2D, mwr_logo_texture);

		Game* gm = game();
		int w = gm->width();
		int hw = w / 2;
		int h = gm->height();
		int hh = h / 2;

		//Max size is 12, but if the window is smaller (width OR height), we should go with the smaller value.
		int imgs = 512;

		if(w < imgs){
			imgs = w;
		}
		if(h < imgs){
			imgs = h;
		}

		int himgs = imgs / 2;

		int img_x = hw - himgs;
		int img_y = hh - himgs;
		int img_width = hw + himgs;
		int img_height = hh + himgs;

		int tickUntil2 = 50;
		int tickUntil3 = 55;
		int tickUntil4 = 70;

		if(ticks > tickUntil2){ //Stage 2
			if(ticks > (tickUntil2 + 10)){ //Stage 3
				if((ticks - tickUntil4) > tickUntil4){
					//TODO: Switch to menu state
					return;
				}

				float i = ((ticks - (tickUntil2 + 10)) - tickUntil3) * 0.1;

				if(i > 1){
					i = 1;
				}

				glColor4f(1, 0, 0, 1 - i);
			}else{ // Actuall do stage 2
				float i = (ticks - tickUntil2) * 0.1;

				if(i > 1){
					i = 1;
				}

				glColor4f(1, 1 - i, 1 - i, 1);
			}
		}else{ //Stage 1
			glColor4f(1, 1, 1, 1);
		}
		glBegin(GL_QUADS);
		{
			glTexCoord2i(0, 0); glVertex2i(img_x, img_y);
			glTexCoord2i(1, 0); glVertex2i(img_width, img_y);
			glTexCoord2i(1, 1); glVertex2i(img_width, img_height);
			glTexCoord2i(0, 1); glVertex2i(img_x, img_height);
		}
		glEnd();

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void IntroState::resize(int width, int height){
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		glOrtho(0, width, height, 0, 1, -1);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glViewport(0.f, 0.f, width, height);
	}
}

#endif
