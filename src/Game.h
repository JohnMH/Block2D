/*
 * Copyright 2015 John M. Harris, Jr.
 *
 * This file is part of Block2D.
 *
 * Block2D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Block2D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Block2D.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAME_H_
#define GAME_H_

#include "GameState.h"

#ifdef QT_CORE_LIB
#include "QtResourceManager.h"
#else
#include "SDLResourceManager.h"
#endif

namespace block2d{
	class Game{
		public:
			Game();
			virtual ~Game();

			bool isRunning();
			void stop();

			void tick();
			void render();

			#ifndef BLOCK2D_SERVER
			void resize(int width, int height);
			int width();
			int height();
			#endif

			GameState* state();

			GameState* swapState(GameState* newState);

			ResourceManager* getResourceManager();

		private:
			bool running;

			#ifndef BLOCK2D_SERVER
			int _width;
			int _height;
			#endif

			ResourceManager* res_mgr;

			GameState* gs;
			bool stateInited;
	};
}

#endif
