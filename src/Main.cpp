/*
 * Copyright 2015 John M. Harris, Jr.
 *
 * This file is part of Block2D.
 *
 * Block2D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Block2D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Block2D.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <cstdlib>

#ifdef EMSCRIPTEN
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#endif

#include "Game.h"
#include "IntroState.h"

void b2d_shutdown(){
	SDL_Quit();
	#ifndef BLOCK2D_SERVER
	IMG_Quit();
	#endif
}

int main(int argc, char* argv[]){
	(void)argc;
	(void)argv;

	#ifdef BLOCK2D_SERVER

	Uint32 sdl_init_flags = 0;

	#else

	Uint32 sdl_init_flags = SDL_INIT_VIDEO;

	#endif

	if(SDL_Init(sdl_init_flags) != 0){
		std::cout << "SDL_Init Failed: " << SDL_GetError() << std::endl;
		return EXIT_FAILURE;
	}

	#ifndef BLOCK2D_SERVER
	int img_init_flags = IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF | IMG_INIT_WEBP;
	if(IMG_Init(img_init_flags) != img_init_flags){
		std::cout << "IMG_Init Failed: " << IMG_GetError() << std::endl;
		SDL_Quit();
		return EXIT_FAILURE;
	}

	SDL_Window* win = SDL_CreateWindow("Block2D", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	if(!win){
		std::cout << "SDL_CreateWindow Failed: " << SDL_GetError() << std::endl;
		b2d_shutdown();
	}

	SDL_GLContext ctx = SDL_GL_CreateContext(win);
	if(!ctx){
		std::cout << "SDL_CreateContext Failed: " << SDL_GetError() << std::endl;
		SDL_DestroyWindow(win);
		b2d_shutdown();
	}
	#endif

	block2d::Game game;
	#ifndef BLOCK2D_SERVER
	game.resize(640, 480);
	#endif

	#ifdef BLOCK2D_SERVER
	//Set state to ServerState
	#else
	//Set window icon
	game.swapState(new block2d::IntroState(&game));
	#endif

	while(game.isRunning()){
		SDL_Event evt;
		if(SDL_WaitEventTimeout(&evt, 25) == 1){
			switch(evt.type){
				case SDL_WINDOWEVENT: {
					switch(evt.window.event){
						case SDL_WINDOWEVENT_CLOSE: {
							game.stop();
							break;
						}
						//TODO: Support resizing window
					}
					break;
				}
				case SDL_KEYDOWN: {
					block2d::GameState* gs = game.state();
					if(gs){
						gs->keyDown(evt.key.keysym.sym);
					}
					break;
				}
				case SDL_KEYUP: {
					block2d::GameState* gs = game.state();
					if(gs){
						gs->keyUp(evt.key.keysym.sym);
					}
					break;
				}
				default: {
					break;
				}
			}
		}

		try{
			game.tick();
			#ifndef BLOCK2D_SERVER
			game.render();
			#endif
		}catch(std::exception &e){
			#if !defined(BLOCK2D_SERVER) && !defined(EMSCRIPTEN) //TODO: Call alert() with Emscripten
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Runtime Exception", e.what(), NULL);
			#endif

			std::cout << "Runtime Exception Encountered: " << e.what() << std::endl;

			break;
		}catch(...){
			#if !defined(BLOCK2D_SERVER) && !defined(EMSCRIPTEN) //TODO: Call alert() with Emscripten
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Runtime Exception", "An unknown error occured.", NULL);
			#endif

			std::cout << "An unknown error occured." << std::endl;

			break;
		}
		#ifndef BLOCK2D_SERVER
		SDL_GL_SwapWindow(win);
		#endif
	}

	#ifndef BLOCK2D_SERVER
	SDL_GL_DeleteContext(ctx);
	#endif

	return EXIT_SUCCESS;
}
