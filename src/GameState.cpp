/*
 * Copyright 2015 John M. Harris, Jr.
 *
 * This file is part of Block2D.
 *
 * Block2D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Block2D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Block2D.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GameState.h"

#include <iostream>

namespace block2d{
	GameState::GameState(Game* game){
		this->_game = game;
	}

	GameState::~GameState(){}

	Game* GameState::game(){
		return _game;
	}

	void GameState::pretick(){}
	void GameState::posttick(){}

	void GameState::prerender(){}
	void GameState::postrender(){}

	void GameState::resize(int width, int height){}

	void GameState::init(){}

	void GameState::keyDown(SDL_Keycode keycode){
		(void)keycode;
	}

	void GameState::keyUp(SDL_Keycode keycode){
		(void)keycode;
	}

	void GameState::switchedState(GameState* newState){
		(void)newState;
	}

	void GameState::quitRequested(){}
}
