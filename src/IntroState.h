/*
 * Copyright 2015 John M. Harris, Jr.
 *
 * This file is part of Block2D.
 *
 * Block2D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Block2D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Block2D.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INTROSTATE_H_
#define INTROSTATE_H_

#include "GameState.h"

#ifndef BLOCK2D_SERVER

#ifdef EMSCRIPTEN
#include <SDL_opengl.h>
#else
#include <SDL2/SDL_opengl.h>
#endif

namespace block2d{
	class IntroState: public GameState{
		public:
			IntroState(Game* gm);
			virtual ~IntroState();

			void init();
			void tick();
			void render();
			void resize(int width, int height);

		private:
			GLuint mwr_logo_texture;
			long ticks;
	};
}

#endif
#endif
