#CONFIG -= qt
CONFIG += debug
QT = core
win32{
	LIBS += -lws2_32
	TARGET = Block2D
}else{
	LIBS += -lGL
	TARGET = block2d
}
LIBS += -lopenal -lalut -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_net

RESOURCES += internalrc.qrc
include(shared.pri)
